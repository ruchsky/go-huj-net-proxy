package main

import (
	"../config"
	"../proxy_core"
	"encoding/binary"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	//proxy_core.PrintWelcome()

	config.InitClientConfig()
	for _, proxyHost := range config.GlobalConfig.ClientProxyHosts {
		go handleProxyPort(strings.TrimSpace(proxyHost))
	}
	select {}
}

/**
  处理客户端代理的端口 端口跟阿里云开启的端口保持一致 当拨号阿里云端口的时候
  把端口上传到阿里云服务端，由服务端进行开启对外tcp端口 保持长连接
  服务端将外部请求路由到客户端
*/
func handleProxyPort(proxyHost string) {
	proxyPort, _ := strconv.Atoi(strings.Split(proxyHost, ":")[1])
	serverUrl := config.GlobalConfig.ServerUrl

	connChan := make(chan net.Conn)
	flagChan := make(chan bool)

	// 拨号
	go func(connCh chan net.Conn, flagCh chan bool) {
		for {
			select {
			case <-flagCh:
				go func(ch chan net.Conn) {
					conn := dial(serverUrl)
					if conn == nil {
						return
					}
					if requestConn(conn, proxyPort) {
						ch <- conn
						return
					}
					log.Println("Bridge connection interrupted,", proxyPort)
					_ = conn.Close()
					flagCh <- true
				}(connCh)
			}
		}
	}(connChan, flagChan)

	// 连接
	go func(connCh chan net.Conn, flagCh chan bool) {
		for {
			select {
			case cn := <-connCh:
				go func(conn net.Conn) {
					localConn := dial(proxyHost)
					if localConn == nil {
						_ = conn.Close()
						flagCh <- true // 通知创建连接
						return
					}
					flagCh <- true // 通知创建连接
					proxy_core.ProxySwap(localConn, conn)
				}(cn)
			}
		}
	}(connChan, flagChan)

	// 初始化连接
	flagChan <- true
}

func requestConn(conn net.Conn, proxyPort int) bool {

	// 请求之前发送token 服务端校验token， 如果成功后面回写1 如果回写-1表示失败
	_, err := conn.Write([]byte(config.GlobalConfig.Token))
	if err != nil {
		log.Println("Fail to set write deadline", err)
		return false
	}

	var connFlag int32
	//等待服务端 回写连接状态
	if err := binary.Read(conn, binary.BigEndian, &connFlag); err != nil {
		log.Println("Fail to set read deadline", err)
		return false
	}

	if connFlag == -1 {
		log.Panic("illegal token")
		return false
	}

	port := int32(proxyPort)
	//连接后先发送port  告诉服务端监听端口
	if !proxy_core.WritePort(conn, port) {
		return false
	}
	var resp int32
	//等待服务端接收到代理请求 响应发送port
	if err := binary.Read(conn, binary.BigEndian, &resp); err != nil {
		return false
	}
	log.Println("proxy service", port)
	return true
}

func dial(dialUrl string) net.Conn {
	log.Printf("client start dial > url = %s", dialUrl)
	for {
		conn, err := net.Dial("tcp", dialUrl)
		if err == nil {
			log.Printf("dial success > url = %s", dialUrl)
			return conn
		}
		log.Printf("dial failure > url = %s, errmsg = %s\n", dialUrl, err.Error())
		time.Sleep(5 * time.Second)
	}
}
